<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<style>
.button {
color: #fff;
text-transform: uppercase;
text-decoration: none;
background: #DC143C;
padding: 20px;
border-radius: 50px;
display: inline-block;
border: none;
transition: all 0.4s ease 0s;
}

.button:hover {
text-shadow: 0px 0px 6px rgba(255, 255, 255, 1);
-webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
-moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
transition: all 0.4s ease 0s;
}

input[type=text] {
  width: 50%;
  padding: 8px 15px;
  margin: 8px 0;
  box-sizing: border-box;
  border: none;
  border-bottom: 2px solid red;
}
</style>

<div class="ui segment">

	<h3>Add a Student</h3>

	<form:form action="saveFoititis" modelAttribute="foititis" method="POST" class="ui form">
		<div class="field">
			<label>First Name</label> 
			<form:input path="firstName" type="text"/>
		</div>
		<div class="field">
			<label>Last Name</label>
			<form:input path="lastName" type="text"/>
		</div>
		<div class="field">
			<label>Email</label>
			<form:input path="email" type="text"/>
		</div>
		<div class="field">
			<label>Semester</label> 
			<form:input path="semester" type="text"/>
		</div>
		<div class="field">
			<label>Non_passed_subjects</label> 
			<form:input path="non_passed_subjects" type="text"/>
		</div>
		<button class="button" type="submit">Save</button>
	</form:form>

</div>