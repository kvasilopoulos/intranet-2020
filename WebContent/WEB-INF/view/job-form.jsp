<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<style>
.button {
color: #fff;
text-transform: uppercase;
text-decoration: none;
background: #DC143C;
padding: 20px;
border-radius: 50px;
display: inline-block;
border: none;
transition: all 0.4s ease 0s;
}

.button:hover {
text-shadow: 0px 0px 6px rgba(255, 255, 255, 1);
-webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
-moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
transition: all 0.4s ease 0s;
}

input[type=text] {
  width: 50%;
  padding: 8px 15px;
  margin: 8px 0;
  box-sizing: border-box;
  border: none;
  border-bottom: 2px solid red;
}
</style>

<div class="ui segment">

	<h3>Add a Job</h3>

	<form:form action="saveJob" modelAttribute="job" method="POST" class="ui form">
		<div class="field">
			<label>Company</label> 
			<form:input path="company" type="text"/>
		</div>
		<div class="field">
			<label>Subject</label>
			<form:input path="subject" type="text"/>
		</div>
		<div class="field">
			<label>Seats</label> 
			<form:input path="seats" type="text"/>
		</div>
		<button class="button" type="submit">Save</button>
	</form:form>

</div>