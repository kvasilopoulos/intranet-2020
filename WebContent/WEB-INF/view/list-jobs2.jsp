

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Availiable Jobs are</title>
<!-- reference our style sheet -->
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
	<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
	<div id="container">
		<div id="content">
			<!--  add our html table here -->
			<table>
				<tr>
					<th>Company</th>
					<th>Subject</th>
					<th>Seats</th>
				</tr>
				<!-- loop over and print our customers -->
				<c:forEach var="tempjob" items="${job}">
					<tr>
						<td>${tempjob.company}</td>
						<td>${tempjob.subject}</td>
						<td>${tempjob.seats}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

</body>
</html>
