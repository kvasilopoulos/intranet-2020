

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>List Foitites</title>
<!-- reference our style sheet -->
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<h2>Student's List</h2>
		</div>
	</div>

	<div id="container">
		<div id="content">
			<!--  add our html table here -->
			<table>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Semester</th>
					<th>Non passed subjects</th>
					<th>First option</th>
					<th>Second option</th>
				</tr>
				<!-- loop over and print our customers -->
				<c:forEach var="tempFoititis" items="${foititis}">

					<tr>
						<td>${tempFoititis.firstName}</td>
						<td>${tempFoititis.lastName}</td>
						<td>${tempFoititis.email}</td>
						<td>${tempFoititis.semester}</td>
						<td>${tempFoititis.non_passed_subjects}</td>
						<td>${tempFoititis.firstoption}</td>
						<td>${tempFoititis.secondoption}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

</body>
</html>
