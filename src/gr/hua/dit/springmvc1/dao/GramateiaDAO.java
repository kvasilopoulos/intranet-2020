package gr.hua.dit.springmvc1.dao;

import java.util.List;


import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

public interface GramateiaDAO {
	
	public List<Foititis> getFoitites();
	
	public void saveFoititis(Foititis foititis);
	
	public Foititis getFoititis(int id);

	public void deleteFoititis(int id);
	
	public List<Job> getJobs();
	
	public void saveJob(Job job);
	
	public Job getJob(int id);

	public void deleteJob(int id);

	}
