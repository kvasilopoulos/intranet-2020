package gr.hua.dit.springmvc1.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

@Repository
public class GramateiaDAOImpl implements GramateiaDAO {

	// inject the session factory
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public List<Job> getJobs() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query
		Query<Job> query = currentSession.createQuery("from Job", Job.class);

		// execute the query and get the results list
		List<Job> jobs = query.getResultList();

		// return the results
		return jobs;
	}
	
	@Override
	@Transactional
	public void saveJob(Job job) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		if (job.getId() != 0) {
			// update the customer
			currentSession.update(job);
		} else {
			// save the student
			currentSession.save(job);
		}

	}
	
	@Override
	@Transactional
	public List<Foititis> getFoitites() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query
		Query<Foititis> query = currentSession.createQuery("from Foititis", Foititis.class);

		// execute the query and get the results list
		List<Foititis> foitites = query.getResultList();

		// return the results
		return foitites;
	}
	
	@Override
	@Transactional
	public void saveFoititis(Foititis foititis) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		if (foititis.getId() != 0) {
			// update the customer
			currentSession.update(foititis);
		} else {
			// save the student
			currentSession.save(foititis);
		}

	}
	
	@Override
	@Transactional
	public Foititis getFoititis(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// get and return Customer
		Foititis foititis = currentSession.get(Foititis.class, id);
		return foititis;
	}

	@Override
	@Transactional
	public void deleteFoititis(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// find the Customer
		Foititis Foititis = currentSession.get(Foititis.class, id);

		// delete Customer
		currentSession.delete(Foititis);

	}
	
	@Override
	@Transactional
	public Job getJob(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// get and return Customer
		Job job = currentSession.get(Job.class, id);
		return job;
	}

	@Override
	@Transactional
	public void deleteJob(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// find the Customer
		Job job = currentSession.get(Job.class, id);

		// delete Customer
		currentSession.delete(job);

	}

}
