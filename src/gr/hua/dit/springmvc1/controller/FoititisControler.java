package gr.hua.dit.springmvc1.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import gr.hua.dit.springmvc1.dao.FoititisDAO;
import gr.hua.dit.springmvc1.dao.GramateiaDAO;
import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

@Controller
@RequestMapping("/foititis")
public class FoititisControler {

	// inject the foitites dao
	@Autowired
	private FoititisDAO foititisDAO;
	
	@GetMapping("/checkSemester")
	public String checksemester(Model model) {
		// create model attribute to get form data
		Foititis foititis = new Foititis();
		model.addAttribute("foititis", foititis);
		
		// add page title
		model.addAttribute("pageTitle", "Check semester");
		return "checksemester";
	}
	
	@PostMapping("/checkedsemester")
	public String checkedsemester(HttpServletRequest request,HttpServletResponse res,Model model) throws ServletException,IOException{
		// get Users from DAO
		String s = request.getParameter("id");
		int id = Integer.parseInt(s);
		String exist = foititisDAO.checksemester(id);
		model.addAttribute("exist",exist);
		return "redirect:/foititis/checkSemester";
	}

	
	@GetMapping("/choiceFoititis")
	public String showchoiceFoititis(Model model) {
		List<Job> jobs = foititisDAO.getJobs();

		// add the foitites to the model
		model.addAttribute("job", jobs);

		Foititis foititis = new Foititis();
		model.addAttribute("foititis", foititis);
		
		// add page title
		model.addAttribute("pageTitle", "Choose the desired job");
		return "availiable-jobs";
	}
	
	@PostMapping("/saveChoice")
	public String saveChoice(HttpServletRequest request,HttpServletResponse res,Model model) throws ServletException,IOException{
		// get Users from DAO
				String s = request.getParameter("id");
				String firstoption = request.getParameter("firstoption");
				String secondoption = request.getParameter("secondoption");
				int id = Integer.parseInt(s);
				foititisDAO.saveChoice(id,firstoption,secondoption);
				return "redirect:/";
	}
	
	@GetMapping("/checkNon_passed_subjects")
	public String checknon_passed_subjects(Model model) {
		// create model attribute to get form data
		Foititis foititis = new Foititis();
		model.addAttribute("foititis", foititis);
		
		// add page title
		model.addAttribute("pageTitle", "Check non_passed_subjects");
		return "checknon_passed_subjects";
	}
	
	@PostMapping("/checkednon_passed_subjects")
	public String checkednon_passed_subjects(HttpServletRequest request,HttpServletResponse res,Model model) throws ServletException,IOException{
		// get Users from DAO
		String s = request.getParameter("id");
		int id = Integer.parseInt(s);
		String exist = foititisDAO.checknon_passed_subjects(id);
		model.addAttribute("exist",exist);
		return "redirect:/foititis/checkNon_passed_subjects";
	}
	
	@GetMapping("/checkFinal")
	public String checkfinal(Model model) {
		// create model attribute to get form data
		Foititis foititis = new Foititis();
		model.addAttribute("foititis", foititis);
		
		// add page title
		model.addAttribute("pageTitle", "Check final");
		return "final";
	}
	
	@PostMapping("/checkedfinal")
	public String checkedfinal(HttpServletRequest request,HttpServletResponse res,Model model) throws ServletException,IOException{
		// get Users from DAO
		String s = request.getParameter("id");
		int id = Integer.parseInt(s);
		String exist = foititisDAO.checkfinal(id);
		model.addAttribute("exist",exist);
		return "redirect:/foititis/checkFinal";
	}

}
